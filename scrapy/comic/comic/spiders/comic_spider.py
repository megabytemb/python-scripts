from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.contrib.linkextractors import LinkExtractor

import scrapy


class ComicItem(scrapy.Item):
    url = scrapy.Field()
    comic_image = scrapy.Field()

class ComicSpider(CrawlSpider):
    name = 'comic'
    allowed_domains = ['explosm.net']
    start_urls = ['http://explosm.net/comics/latest']
    rules = [Rule(LinkExtractor(allow=['/comics/\d+']), 'parse_comic', follow= True)]

    def parse_comic(self, response):
        comic = ComicItem()
        comic['url'] = response.url
        comic['comic_image'] = response.xpath("//img[@id='main-comic']/@src").extract()
        return comic
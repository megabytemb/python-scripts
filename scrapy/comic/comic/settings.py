# -*- coding: utf-8 -*-

# Scrapy settings for comic project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'comic'

SPIDER_MODULES = ['comic.spiders']
NEWSPIDER_MODULE = 'comic.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'comic (+http://www.yourdomain.com)'

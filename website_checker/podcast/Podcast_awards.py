import requests
import smtplib

from pprint import pprint
from BeautifulSoup import BeautifulSoup
import sys
import time
from datetime import datetime, date
from random import choice, randrange
import Podcast_awards_email

def vote(name, email):
    response = requests.get("http://podcastawards.com/index.php")
    soup = BeautifulSoup(response.text)
    securecheck = soup.find('input', {'name': 'securecheck'}).get('value')

    cookies = response.cookies

    data = {
    'cat_11':312,
    'securecheck':securecheck,
    'doItNow':'voted12',
    'enduser_name':name,
    'enduser_addy':email,
    'enduser_type':1,
    'enduser_comment':''
    }
    time.sleep(1)
    response = requests.post('http://podcastawards.com/index.php?option=vote', data=data, cookies=cookies)
    if "Thank You for your Vote!" in response.text:
        return True
    else:
        return False


domains = (
    "michaelthe.guru",
    "thefirst.guru",
    "thelast.guru",
    "megabytemb.com",
    "420justblaze.com",
    "megabytemb.net",
)

people = (
    "Emma Ryan",
    "John Narm",
    "Jake Smith",
    "Jeff Ross",
    "Tony World",
    "Andrew Hits",
    "Sunny 88p",
    "Rita repulsa",
    "Ivan Von Ooze",
    "Gabriel Belmont",
    "Blue Bartech",
    "Mike krune",
    "Andy Joy",
    "Luke skywalker",
    "Obi-wan kenobi",
    "Tony stark",
)

if datetime.now() >= datetime(2015, 3, 23,0,0,0):
    sys.exit()
waittime = randrange(0,1800)
time.sleep(waittime)
person = choice(people)
email = person.split(" ")[randrange(0,2)] + "@" + choice(domains)
result = vote(person, email)
if result == True:
    result = Podcast_awards_email.check_email()
    if result == "Success":
        response = "Vote Succeeded and Verified using: " + person + ";" + email
    else:
        response = "Vote Succeeded and Not Verified using: " + person + ";" + email
else:
    response = "Vote failed using: " + person + ";" + email


sender = 'server@megabytemb.com'
receivers = ['michael@megabytemb.com']

message = """From: Server <server@megabytemb.com>
To: Michael <michael@benz.io>
MIME-Version: 1.0
Content-type: text/html
Subject: %s

%s
""" % (response, response)
try:
   smtpObj = smtplib.SMTP('localhost')
   smtpObj.sendmail(sender, receivers, message)         
except SMTPException:
   print "Error: unable to send email"
import httplib2
import email
import base64
from BeautifulSoup import BeautifulSoup
import requests

from apiclient.discovery import build
from oauth2client.client import flow_from_clientsecrets
from oauth2client.file import Storage
from oauth2client.tools import run


# Path to the client_secret.json file downloaded from the Developer Console
CLIENT_SECRET_FILE = 'client_secrets.json'

# Check https://developers.google.com/gmail/api/auth/scopes for all available scopes
OAUTH_SCOPE = 'https://www.googleapis.com/auth/gmail.modify'

# Location of the credentials storage file
STORAGE = Storage('gmail.storage.V2')

# Start the OAuth flow to retrieve credentials
flow = flow_from_clientsecrets(CLIENT_SECRET_FILE, scope=OAUTH_SCOPE, redirect_uri="urn:ietf:wg:oauth:2.0:oob")
http = httplib2.Http()

# Try to retrieve credentials from storage or run the flow to generate them
credentials = STORAGE.get()
if credentials is None or credentials.invalid:
    authorize_url = flow.step1_get_authorize_url()
    print 'Go to the following link in your browser: ' + authorize_url
    code = raw_input('Enter verification code: ').strip()
    credentials = flow.step2_exchange(code)
    STORAGE.put(credentials)


# Authorize the httplib2.Http object with our credentials
http = credentials.authorize(http)

# Build the Gmail service from discovery
gmail_service = build('gmail', 'v1', http=http)

# Retrieve a page of threads
def check_email():
    messages_meta = gmail_service.users().messages().list(userId='me', 
                                                          q="label:podcastawards and -label:podcastawards-processed"
                                                          ).execute()
    if messages_meta.has_key("messages"):
        for message_meta in messages_meta["messages"]:
            message = gmail_service.users().messages().get(userId="me", id=message_meta["id"], format='raw').execute()
            msg_str = base64.urlsafe_b64decode(message['raw'].encode('ASCII'))
            msg = email.message_from_string(msg_str)
            for part in msg.walk():
                msg.get_payload()
                if part.get_content_type() == 'text/html':
                    # print part.get_payload()
                    soup = BeautifulSoup(part.get_payload())
                    url=soup.find('a').get('href')
                    response = requests.get(url)
                    if "Your vote has been verified." in response.text:
                        gmail_service.users().messages().modify(userId="me", 
                                                                id=message_meta["id"], 
                                                                body={"addLabelIds": ['Label_19']}
                                                                ).execute()
                        return "Success"
                    else:
                        return "Fail"
                else:
                    "return Fail"
    else:
        return "No Email to Process"